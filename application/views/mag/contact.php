<!DOCTYPE html>
<html lang="en">
<head>
  <title>EduPack | Contact</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="">
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700%7CSource+Sans+Pro:400,600,700' rel='stylesheet'>
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/bootstrap.min.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/font-icons.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/style.css')?>" />
  <link rel="shortcut icon" href="<?=base_url('assets/mag/img/favicon.ico')?>">
  <link rel="apple-touch-icon" href="<?=base_url('assets/mag/img/apple-touch-icon.php')?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('assets/mag/img/apple-touch-icon-72x72.png')?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/mag/img/apple-touch-icon-114x114.png')?>">
  <script src="<?=base_url('assets/mag/js/lazysizes.min.js')?>"></script>
</head>

<body class="style-default style-rounded">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      <div></div>
    </div>
  </div>
  
  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <?php include('includes/sidenav.php'); ?>


  <main class="main oh" id="main">

    <?php include('includes/top_bar.php'); ?>
    
    <?php include('includes/header.php'); ?>

    <!-- Breadcrumbs -->
    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="<?=base_url('Magazine/index')?>" class="breadcrumbs__url">Home</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          Contact
        </li>
      </ul>
    </div>

    <div class="main-container container" id="main-container">            
      <!-- post content -->
      <div class="blog__content mb-72">
        <h1 class="page-title">Contact Me</h1>
 
        <div class="row justify-content-center">
          <div class="col-lg-8">
            <h4>Drop Us a Message</h4>
            <p>Don't hesitate to get in touch. I'll reply you as soon as possible.</p>
            <ul class="contact-items">
              <li class="contact-item"><address>Raipur Chhattisgarh 492001</address></li>
              <li class="contact-item"><a href="tel:8839826466">8839826466</a></li>
              <li class="contact-item"><a href="mailto:amitchandrakar028@gmail.com">amitchandrakar028@gmail.com</a></li>
            </ul>            

            <!-- Contact Form -->
            <form id="contact-form" class="contact-form mt-30 mb-30" method="post" action="#">
              <div class="contact-name">
                <label for="name">Name <abbr title="required" class="required">*</abbr></label>
                <input name="name" id="name" type="text">
              </div>
              <div class="contact-email">
                <label for="email">Email <abbr title="required" class="required">*</abbr></label>
                <input name="email" id="email" type="email">
              </div>
              <div class="contact-subject">
                <label for="email">Subject</label>
                <input name="subject" id="subject" type="text">
              </div>
              <div class="contact-message">
                <label for="message">Message <abbr title="required" class="required">*</abbr></label>
                <textarea id="message" name="message" rows="7" required="required"></textarea>
              </div>

              <input type="submit" class="btn btn-lg btn-color btn-button" value="Send Message" id="submit-message">
              <div id="msg" class="message"></div>
            </form>

          </div>
        </div>
      </div> <!-- end post content -->
    </div> <!-- end main container -->

    <?php include('includes/footer.php'); ?>

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->
  
  <!-- jQuery Scripts -->
  <script src="<?=base_url('assets/mag/js/jquery.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/bootstrap.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/easing.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/owl-carousel.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/flickity.pkgd.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/twitterFetcher_min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/jquery.newsTicker.min.js')?>"></script>  
  <script src="<?=base_url('assets/mag/js/modernizr.min.js')?>"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoQ3_zzRfW-hYspkwr5kvwCwLPGZsN4nw"></script> -->
  <script src="<?=base_url('assets/mag/js/gmap3.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/scripts.js')?>"></script>

  <!-- Google Map -->
  <script type="text/javascript">
    $(document).ready( function(){

      var gmapDiv = $("#google-map");
      var gmapMarker = gmapDiv.attr("data-address");

      gmapDiv.gmap3({
        zoom: 16,
        address: gmapMarker,
        oomControl: true,
        navigationControl: true,
        scrollwheel: false,
        styles: [
          {
          "featureType":"all",
          "elementType":"all",
            "stylers":[
              { "saturation":"0" }
            ]
        }]
      })
      .marker({
        address: gmapMarker,
        icon: "img/map_pin.png"
      })
      .infowindow({
        content: "V Tytana St, Manila, Philippines"
      })
      .then(function (infowindow) {
        var map = this.get(0);
        var marker = this.get(1);
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
      });
    });
  </script>
</body>
</html>