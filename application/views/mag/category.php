<!DOCTYPE html>
<html lang="en">
<head>
  <title>EduPack | Category</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="">
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700%7CSource+Sans+Pro:400,600,700' rel='stylesheet'>
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/bootstrap.min.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/font-icons.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/style.css')?>" />
  <link rel="shortcut icon" href="<?=base_url('assets/mag/img/favicon.ico')?>">
  <link rel="apple-touch-icon" href="<?=base_url('assets/mag/img/apple-touch-icon.html')?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('assets/mag/img/apple-touch-icon-72x72.png')?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/mag/img/apple-touch-icon-114x114.png')?>">
  <script src="<?=base_url('assets/mag/js/lazysizes.min.js')?>"></script>
</head>
<body class="bg-light style-default style-rounded">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      <div></div>
    </div>
  </div>

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <!-- Sidenav -->    
  <?php include('includes/sidenav.php'); ?>
  <!-- end sidenav -->

  <main class="main oh" id="main">

    <!-- Top Bar -->
    <?php include('includes/top_bar.php'); ?>
    <?php include('includes/header.php'); ?>
    <!-- end top bar -->        

    <div class="main-container container pt-40" id="main-container">         

      <!-- Content -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content mb-72">
          <h6 class="page-title">
            <?php
             foreach ($bbb as $a) 
             {
               echo $cat_name1 = $a->cat_name;
             }
            ?>
  
          </h6>


          <?php
             foreach ($aaa as $value1) 
             { ?>            

          <article class="entry card post-list">
            <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url(<?=base_url('images/post/'.$image1 = $value1->image)?>)">
              <a href="<?=base_url('Magazine/single_post/'.$value1->post_id)?>" class="thumb-url"></a>
              <img src="img/content/list/list_post_1.jpg" alt="" class="entry__img d-none">
              <a href="categories.html" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--blue"><?=$value1->cat_name?></a>
            </div>

            <div class="entry__body post-list__body card__body">
              <div class="entry__header">
                <h2 class="entry__title">
                  <a href="<?=base_url('Magazine/single_post/'.$value1->post_id)?>"><?=$value1->title?></a>
                </h2>
                <ul class="entry__meta">
                  <li class="entry__meta-author">
                    <span>by</span>
                    <a href="#"><?=$value1->user_name?></a>
                  </li>
                  <li class="entry__meta-date">
                    <?=date('d-M-Y H:i A', strtotime($value1->created_at))?>
                  </li>
                </ul>
              </div>        
              <div class="entry__excerpt">
                <p><?=substr($value1->description, 0, 70)?> ... <a href="<?=base_url('Magazine/single_post/'.$value1->post_id)?>">Read More</a></p>
              </div>
            </div>
          </article>

          <?php } ?>

          
          <!-- Pagination -->
          <nav class="pagination">
            <span class="pagination__page pagination__page--current">1</span>
            <a href="#" class="pagination__page">2</a>
            <a href="#" class="pagination__page">3</a>
            <a href="#" class="pagination__page">4</a>
            <a href="#" class="pagination__page pagination__icon pagination__page--next"><i class="ui-arrow-right"></i></a>
          </nav>
        </div> <!-- end posts -->

        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">

          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Popular Posts</h4>
            <ul class="post-list-small">
              

              <?php
              foreach ($g as $post) 
              { ?>
              
                <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="<?=base_url('Magazine/single_post/'.$post->post_id)?>">
                        <img data-src="<?=base_url('images/post/'.$post->image)?>" src="<?=base_url('images/post/'.$post->image)?>" alt="" class="post-list-small__img--rounded lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="<?=base_url('Magazine/single_post/'.$post->post_id)?>"><?=$post->title?></a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#"><?=$post->user_name?></a>
                      </li> 
                      <li class="entry__meta-date">
                        <?=date('d-M-Y H:i A', strtotime($post->created_at))?>
                      </li>
                    </ul>
                  </div>                  
                </article>
              </li>

              <?php } ?>



              
            </ul>           
          </aside> <!-- end widget popular posts -->

          <!-- Widget Newsletter -->
          <aside class="widget widget_mc4wp_form_widget">
            <h4 class="widget-title">Newsletter</h4>
            <p class="newsletter__text">
              <i class="ui-email newsletter__icon"></i>
              Subscribe for our daily news
            </p>
            <form class="mc4wp-form" method="post">
              <div class="mc4wp-form-fields">
                <div class="form-group">
                  <input type="email" name="EMAIL" placeholder="Your email" required="">
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-lg btn-color" value="Sign Up">
                </div>
              </div>
            </form>
          </aside> <!-- end widget newsletter -->

          <!-- Widget Socials -->
          <aside class="widget widget-socials">
            <h4 class="widget-title">Let's hang out on social</h4>
            <div class="socials socials--wide socials--large">
              <div class="row row-16">
                <div class="col">
                  <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                    <span class="social__text">Facebook</span>
                  </a><!--
                  --><a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                    <span class="social__text">Twitter</span>
                  </a><!--
                  --><a class="social social-youtube" href="#" title="youtube" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                    <span class="social__text">Youtube</span>
                  </a>
                </div>
                <div class="col">
                  <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                    <i class="ui-google"></i>
                    <span class="social__text">Google+</span>
                  </a><!--
                  --><a class="social social-instagram" href="#" title="instagram" target="_blank" aria-label="instagram">
                    <i class="ui-instagram"></i>
                    <span class="social__text">Instagram</span>
                  </a><!--
                  --><a class="social social-rss" href="#" title="rss" target="_blank" aria-label="rss">
                    <i class="ui-rss"></i>
                    <span class="social__text">Rss</span>
                  </a>
                </div>                
              </div>            
            </div>
          </aside> <!-- end widget socials -->

        </aside> <!-- end sidebar -->
  
      </div> <!-- end content -->
    </div> <!-- end main container -->

    <!-- Footer -->
    <footer class="footer footer--dark">
      <div class="container">
        <div class="footer__widgets">
          <div class="row">

            <div class="col-lg-3 col-md-6">
              <aside class="widget widget-logo">
                <a href="index-2.html">
                  <img src="img/logo_default_white.png" srcset="img/logo_default_white.png 1x, img/logo_default_white@2x.png 2x" class="logo__img" alt="">
                </a>
                <p class="copyright">
                  © 2018 Deus | Made by <a href="https://deothemes.com/">DeoThemes</a>
                </p>
                <div class="socials socials--large socials--rounded mb-24">
                  <a href="#" class="social social-facebook" aria-label="facebook"><i class="ui-facebook"></i></a>
                  <a href="#" class="social social-twitter" aria-label="twitter"><i class="ui-twitter"></i></a>
                  <a href="#" class="social social-google-plus" aria-label="google+"><i class="ui-google"></i></a>
                  <a href="#" class="social social-youtube" aria-label="youtube"><i class="ui-youtube"></i></a>
                  <a href="#" class="social social-instagram" aria-label="instagram"><i class="ui-instagram"></i></a>
                </div>
              </aside>
            </div>

            <div class="col-lg-2 col-md-6">
              <aside class="widget widget_nav_menu">
                <h4 class="widget-title">Useful Links</h4>
                <ul>
                  <li><a href="about.html">About</a></li>
                  <li><a href="contact.html">News</a></li>
                  <li><a href="categories.html">Advertise</a></li>
                  <li><a href="shortcodes.html">Support</a></li>
                  <li><a href="shortcodes.html">Features</a></li>
                  <li><a href="shortcodes.html">Contact</a></li>
                </ul>
              </aside>
            </div>  

            <div class="col-lg-4 col-md-6">
              <aside class="widget widget-popular-posts">
                <h4 class="widget-title">Popular Posts</h4>
                <ul class="post-list-small">
                  <li class="post-list-small__item">
                    <article class="post-list-small__entry clearfix">
                      <div class="post-list-small__img-holder">
                        <div class="thumb-container thumb-100">
                          <a href="single-post.html">
                            <img data-src="img/content/post_small/post_small_1.jpg" src="img/empty.png" alt="" class="lazyload">
                          </a>
                        </div>
                      </div>
                      <div class="post-list-small__body">
                        <h3 class="post-list-small__entry-title">
                          <a href="single-post.html">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                        </h3>
                        <ul class="entry__meta">
                          <li class="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li class="entry__meta-date">
                            Jan 21, 2018
                          </li>
                        </ul>
                      </div>                  
                    </article>
                  </li>
                  <li class="post-list-small__item">
                    <article class="post-list-small__entry clearfix">
                      <div class="post-list-small__img-holder">
                        <div class="thumb-container thumb-100">
                          <a href="single-post.html">
                            <img data-src="img/content/post_small/post_small_2.jpg" src="img/empty.png" alt="" class="lazyload">
                          </a>
                        </div>
                      </div>
                      <div class="post-list-small__body">
                        <h3 class="post-list-small__entry-title">
                          <a href="single-post.html">Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire</a>
                        </h3>
                        <ul class="entry__meta">
                          <li class="entry__meta-author">
                            <span>by</span>
                            <a href="#">DeoThemes</a>
                          </li>
                          <li class="entry__meta-date">
                            Jan 21, 2018
                          </li>
                        </ul>
                      </div>                  
                    </article>
                  </li>
                </ul>
              </aside>              
            </div>

            <div class="col-lg-3 col-md-6">
              <aside class="widget widget_mc4wp_form_widget">
                <h4 class="widget-title">Newsletter</h4>
                <p class="newsletter__text">
                  <i class="ui-email newsletter__icon"></i>
                  Subscribe for our daily news
                </p>
                <form class="mc4wp-form" method="post">
                  <div class="mc4wp-form-fields">
                    <div class="form-group">
                      <input type="email" name="EMAIL" placeholder="Your email" required="">
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-lg btn-color" value="Sign Up">
                    </div>
                  </div>
                </form>                
              </aside>
            </div>

          </div>
        </div>    
      </div> <!-- end container -->
    </footer> <!-- end footer -->

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->
  
  <script src="<?=base_url('assets/mag/js/jquery.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/bootstrap.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/easing.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/owl-carousel.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/flickity.pkgd.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/twitterFetcher_min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/jquery.newsTicker.min.js')?>"></script>  
  <script src="<?=base_url('assets/mag/js/modernizr.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/scripts.js')?>"></script>
</body>
</html>