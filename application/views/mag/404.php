<!DOCTYPE html>
<html lang="en">
<head>
  <title>EduPack | 404</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="">
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700%7CSource+Sans+Pro:400,600,700' rel='stylesheet'>
  <link rel="stylesheet" href="css/bootstrap.min.css" />
  <link rel="stylesheet" href="css/font-icons.css" />
  <link rel="stylesheet" href="css/style.css" />
  <link rel="shortcut icon" href="img/favicon.ico">
  <link rel="apple-touch-icon" href="img/apple-touch-icon.html">
  <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
  <script src="js/lazysizes.min.js"></script>
</head>

<body class="style-default style-rounded error404">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      <div></div>
    </div>
  </div>
  
  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <?php include('includes/sidenav.php'); ?>


  <main class="main oh" id="main">
  
  <?php include('includes/header.php'); ?>

    <div class="main-container container pt-80 pb-80" id="main-container">            
      <!-- post content -->
      <div class="blog__content mb-72">
        <div class="container text-center">
          <h1 class="page-404-number">404</h1>
          <h2>Page not found</h2>
          <p>Don't fret! Let's get you back on track. Perhaps searching can help</p>

        </div> <!-- end container -->
      </div> <!-- end post content -->
    </div> <!-- end main container -->

    <?php include('includes/footer.php'); ?>

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->

  
  <!-- jQuery Scripts -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/easing.min.js"></script>
  <script src="js/owl-carousel.min.js"></script>
  <script src="js/flickity.pkgd.min.js"></script>
  <script src="js/twitterFetcher_min.js"></script>
  <script src="js/jquery.newsTicker.min.js"></script>  
  <script src="js/modernizr.min.js"></script>
  <script src="js/scripts.js"></script>

</body>

<!-- Mirrored from deothemes.com/envato/deus/html/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Feb 2019 09:13:54 GMT -->
</html>