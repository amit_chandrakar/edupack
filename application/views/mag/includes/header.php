    <!-- Navigation -->
    <header class="nav">

      <div class="nav__holder nav--sticky">
        <div class="container relative">
          <div class="flex-parent">

            <!-- Side Menu Button -->
            <button class="nav-icon-toggle" id="nav-icon-toggle" aria-label="Open side menu">
              <span class="nav-icon-toggle__box">
                <span class="nav-icon-toggle__inner"></span>
              </span>
            </button> 

            <!-- Logo -->
            <a href="#" class="logo">
              <!-- <img class="logo__img" src="<?=base_url('assets/mag/img/logo_default.png')?>" alt="logo"> -->
              <h3 style="margin-top: 10px;">EduPack</h3>
            </a>

            <!-- Nav-wrap -->
            <nav class="flex-child nav__wrap d-none d-lg-block">              
              <ul class="nav__menu">


                <li <?php if($this->uri->uri_string()=='Magazine/index'){echo 'class="active"';}?>>
                  <a href="<?=base_url('Magazine/index')?>">Home</a>
                </li>

                <li class="nav__dropdown <?php if($this->uri->uri_string()=='Magazine/category'){echo 'active';}?>">
                  <a href="#">Category</a>
                  <ul class="nav__dropdown-menu">
                    <?php
               foreach ($f as $cat_name) 
               { ?> 
                 <li class=""><a href="<?=base_url('Magazine/category/'.$cat_name->cat_id)?>"><?=$cat_name->cat_name?></a></li>
              <?php }
               ?>
                  </ul>
                </li>  

                <li class="<?php if($this->uri->uri_string()=='Magazine/about'){echo 'active';}?>"><a href="<?=base_url('Magazine/about')?>">About</a></li>
                <li class="<?php if($this->uri->uri_string()=='Magazine/contact'){echo 'active';}?>"><a href="<?=base_url('Magazine/contact')?>">Contact</a></li>
  

              </ul> <!-- end menu -->
            </nav> <!-- end nav-wrap -->

            <!-- Nav Right -->
            <div class="nav__right">

              <!-- Search -->
              <div class="nav__right-item nav__search">
                <a href="#" class="nav__search-trigger" id="nav__search-trigger">
                  <i class="ui-search nav__search-trigger-icon"></i>
                </a>
                <div class="nav__search-box" id="nav__search-box">
                  <form class="nav__search-form">
                    <input type="text" placeholder="Search an article" class="nav__search-input">
                    <button type="submit" class="search-button btn btn-lg btn-color btn-button">
                      <i class="ui-search nav__search-icon"></i>
                    </button>
                  </form>
                </div>                
              </div>             

            </div> <!-- end nav right -->            
        
          </div> <!-- end flex-parent -->
        </div> <!-- end container -->

      </div>
    </header> <!-- end navigation