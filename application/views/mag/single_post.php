<?php
 foreach ($a as $value1) 
 {
   $cat_id1 = $value1->cat_id; 
   $cat_name1 = $value1->cat_name; 
   $title1 = $value1->title; 
   $description1 = $value1->description;
   $image1 = $value1->image;
 }
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>EduPack | <?=$title1?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="">
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700%7CSource+Sans+Pro:400,600,700' rel='stylesheet'>
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/bootstrap.min.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/font-icons.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/style.css')?>" />
  <link rel="shortcut icon" href="<?=base_url('assets/mag/img/favicon.ico')?>">
  <link rel="apple-touch-icon" href="<?=base_url('assets/mag/img/apple-touch-icon.php')?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('assets/mag/img/apple-touch-icon-72x72.png')?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/mag/img/apple-touch-icon-114x114.png')?>">
  <script src="<?=base_url('assets/mag/js/lazysizes.min.js')?>"></script>
</head>

<body class="bg-light single-post style-default style-rounded">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      <div></div>
    </div>
  </div>
  
  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <?php include('includes/sidenav.php'); ?>


  <main class="main oh" id="main">
    <?php include('includes/top_bar.php'); ?>

    <?php include('includes/header.php'); ?>

    <!-- Breadcrumbs -->
    <div class="container">
      <ul class="breadcrumbs">
        <li class="breadcrumbs__item">
          <a href="<?=base_url()?>" class="breadcrumbs__url">Home</a>
        </li>
        <li class="breadcrumbs__item breadcrumbs__item--current">
          <?=ucfirst($value1->cat_name)?>
        </li>
      </ul>
    </div>

    <div class="main-container container" id="main-container">

      <!-- Content -->
      <div class="row">
            
        <!-- post content -->
        <div class="col-lg-8 blog__content mb-72">
          <div class="content-box">           

            <!-- standard post -->
            <article class="entry mb-0">
              
              <div class="single-post__entry-header entry__header">
                <a href="categories.php" class="entry__meta-category entry__meta-category--label entry__meta-category--green"><?=$value1->cat_name?></a>
                <h1 class="single-post__entry-title">
                  <?=$value1->title?>
                </h1>

                <div class="entry__meta-holder">
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>by</span>
                      <a href="#"><?=$value1->user_name?></a>
                    </li>
                    <li class="entry__meta-date">
                      <?=date('d-M-Y H:i A', strtotime($value1->created_at))?>
                    </li>
                  </ul>

                  <!-- <ul class="entry__meta">
                    <li class="entry__meta-views">
                      <i class="ui-eye"></i>
                      <span>1356</span>
                    </li>
                    <li class="entry__meta-comments">
                      <a href="#">
                        <i class="ui-chat-empty"></i>13
                      </a>
                    </li>
                  </ul> -->
                </div>
              </div> <!-- end entry header -->

              <div class="entry__img-holder">
                <img src="img/content/single/single_post_featured_img.jpg" alt="" class="entry__img">
              </div>

              <div class="entry__article-wrap">

                <!-- Share -->
                <div class="entry__share">
                  <div class="sticky-col">
                    <div class="socials socials--rounded socials--large">
                      <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                        <i class="ui-facebook"></i>
                      </a>
                      <a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                        <i class="ui-twitter"></i>
                      </a>
                      <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                        <i class="ui-google"></i>
                      </a>
                      <a class="social social-pinterest" href="#" title="pinterest" target="_blank" aria-label="pinterest">
                        <i class="ui-pinterest"></i>
                      </a>
                    </div>
                  </div>                  
                </div> <!-- share -->

                <div class="entry__article">
                  <?=$value1->description?>
                </div> <!-- end entry article -->
              </div> <!-- end entry article wrap -->
              


              <!-- Prev / Next Post -->
              <nav class="entry-navigation">
                <div class="clearfix">
                  <div class="entry-navigation--left">
                    <i class="ui-arrow-left"></i>
                    <span class="entry-navigation__label">Previous Post</span>
                    <div class="entry-navigation__link">
                      
                        <?php 
                          foreach ($p as $key) 
                          { ?>
                            <a href="<?=base_url('Magazine/single_post/'.$key->post_id)?>" rel="next"><?=$key->title?> </a>
                         <?php }
                        ?>
                    </div>
                  </div>
                  <div class="entry-navigation--right">
                    <span class="entry-navigation__label">Next Post</span>
                    <i class="ui-arrow-right"></i>
                    <div class="entry-navigation__link">
                        <?php 
                          foreach ($n as $key) 
                          { ?>
                            <a href="<?=base_url('Magazine/single_post/'.$key->post_id)?>" rel="next"><?=$key->title?> </a>
                         <?php }
                        ?>
                    </div>
                  </div>
                </div>
              </nav>

            

              <!-- Related Posts -->
              <section class="section related-posts mt-40 mb-0">
                <div class="title-wrap title-wrap--line title-wrap--pr">
                  <h3 class="section-title">Related Articles</h3>
                </div>

                <!-- Slider -->
                <div id="owl-posts-3-items" class="owl-carousel owl-theme owl-carousel--arrows-outside">
                  
                  <?php
                   foreach ($b as $value2) 
                   { ?>
                     <!-- $cat_id2 = $value2->cat_id; 
                     $cat_name2 = $value2->cat_name; 
                     $title2 = $value2->title; 
                     $description2 = $value2->description;
                     $image2 = $value2->image; -->
                     <article class="entry thumb thumb--size-1">
                    <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=base_url('images/post/'.$value2->image)?>');">
                      <div class="bottom-gradient"></div>
                      <div class="thumb-text-holder">   
                        <h2 class="thumb-entry-title">
                          <a href="<?=base_url('Magazine/single_post/'.$value2->post_id)?>"><?=$value2->title?></a>
                        </h2>
                      </div>
                      <a href="<?=base_url('Magazine/single_post/'.$value2->post_id)?>" class="thumb-url"></a>
                    </div>
                  </article>
                  <?php }
                  ?>
                  



                  
                </div> <!-- end slider -->

              </section> <!-- end related posts -->            

            </article> <!-- end standard post -->

     

            <!-- Comment Form -->
            <div id="respond" class="comment-respond" style="display: none;">
              <div class="title-wrap">
                <h5 class="comment-respond__title section-title">Leave a Reply</h5>
              </div>
              <form id="form" class="comment-form" method="post" action="#">
                <p class="comment-form-comment">
                  <label for="comment">Comment</label>
                  <textarea id="comment" name="comment" rows="5" required="required"></textarea>
                </p>

                <div class="row row-20">
                  <div class="col-lg-4">
                    <label for="name">Name: *</label>
                    <input name="name" id="name" type="text">
                  </div>
                  <div class="col-lg-4">
                    <label for="comment">Email: *</label>
                    <input name="email" id="email" type="email">
                  </div>
                  <div class="col-lg-4">
                    <label for="comment">Website:</label>
                    <input name="website" id="website" type="text">
                  </div>
                </div>

                <p class="comment-form-submit">
                  <input type="submit" class="btn btn-lg btn-color btn-button" value="Post Comment" id="submit-message">
                </p>
                
              </form>
            </div> <!-- end comment form -->

          </div> <!-- end content box -->
        </div> <!-- end post content -->
        
        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">

          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Popular Posts</h4>
            <ul class="post-list-small">


              <?php
              foreach ($c as $post) 
              { ?>
              <!-- $cat_id1 = $value1->cat_id; 
              $cat_name1 = $value1->cat_name; 
              $title1 = $value1->title; 
              $description1 = $value1->description;
              $image1 = $value1->image; -->
              <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="<?=base_url('Magazine/single_post/'.$post->post_id)?>">
                        <img data-src="<?=base_url('images/post/'.$post->image)?>" src="<?=base_url('images/post/'.$post->image)?>" alt="" class="post-list-small__img--rounded lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="<?=base_url('Magazine/single_post/'.$post->post_id)?>"><?=$post->title?></a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#"><?=$post->user_name?></a>
                      </li>
                      <li class="entry__meta-date">
                       <?=date('d-M-Y H:i A', strtotime($post->created_at))?>
                      </li>
                    </ul>
                  </div>                  
                </article>
              </li>
             <?php } ?>
              




            </ul>           
          </aside> <!-- end widget popular posts -->

          <!-- Widget Newsletter -->
          <aside class="widget widget_mc4wp_form_widget" style="display: none;">
            <h4 class="widget-title">Newsletter</h4>
            <p class="newsletter__text">
              <i class="ui-email newsletter__icon"></i>
              Subscribe for our daily news
            </p>
            <form class="mc4wp-form" method="post">
              <div class="mc4wp-form-fields">
                <div class="form-group">
                  <input type="email" name="EMAIL" placeholder="Your email" required="">
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-lg btn-color" value="Sign Up">
                </div>
              </div>
            </form>
          </aside> <!-- end widget newsletter -->

          <!-- Widget Socials -->
          <aside class="widget widget-socials">
            <h4 class="widget-title">Let's hang out on social</h4>
            <div class="socials socials--wide socials--large">
              <div class="row row-16">
                <div class="col">
                  <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                    <span class="social__text">Facebook</span>
                  </a><!--
                  --><a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                    <span class="social__text">Twitter</span>
                  </a><!--
                  --><a class="social social-youtube" href="#" title="youtube" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                    <span class="social__text">Youtube</span>
                  </a>
                </div>
                <div class="col">
                  <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                    <i class="ui-google"></i>
                    <span class="social__text">Google+</span>
                  </a><!--
                  --><a class="social social-instagram" href="#" title="instagram" target="_blank" aria-label="instagram">
                    <i class="ui-instagram"></i>
                    <span class="social__text">Instagram</span>
                  </a><!--
                  --><a class="social social-rss" href="#" title="rss" target="_blank" aria-label="rss">
                    <i class="ui-rss"></i>
                    <span class="social__text">Rss</span>
                  </a>
                </div>                
              </div>            
            </div>
          </aside> <!-- end widget socials -->

        </aside> <!-- end sidebar -->
      
      </div> <!-- end content -->
    </div> <!-- end main container -->

    <?php include('includes/footer.php'); ?>

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->

  
  <!-- jQuery Scripts -->
  <script src="<?=base_url('assets/mag/js/jquery.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/bootstrap.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/easing.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/owl-carousel.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/flickity.pkgd.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/twitterFetcher_min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/jquery.sticky-kit.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/jquery.newsTicker.min.js')?>"></script>  
  <script src="<?=base_url('assets/mag/js/modernizr.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/scripts.js')?>"></script>

</body>

<!-- Mirrored from Admin.com/envato/deus/html/single_post.php by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 21 Feb 2019 09:14:05 GMT -->
</html>