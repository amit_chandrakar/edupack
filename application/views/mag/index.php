<?php
 foreach ($a as $value1) 
 {
   $cat_id1 = $value1->cat_id; 
   $cat_name1 = $value1->cat_name; 
   $title1 = $value1->title; 
   $description1 = $value1->description;
   $image1 = $value1->image;
 }
 foreach ($b as $value2) 
 {
   $cat_id2 = $value2->cat_id; 
   $cat_name2 = $value2->cat_name; 
   $title2 = $value2->title; 
   $description2 = $value2->description;
   $image2 = $value2->image;
 }
 foreach ($c as $value3) 
 {
   $cat_id3 = $value3->cat_id; 
   $cat_name3 = $value3->cat_name; 
   $title3 = $value3->title; 
   $description3 = $value3->description;
   $image3 = $value3->image;
 }
 foreach ($d as $value4) 
 {
   $cat_id4 = $value4->cat_id; 
   $cat_name4 = $value4->cat_name; 
   $title4 = $value4->title; 
   $description4 = $value4->description;
   $image4 = $value4->image;
 }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>EduPack | Home</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="description" content="">
  <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700%7CSource+Sans+Pro:400,600,700' rel='stylesheet'>
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/bootstrap.min.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/font-icons.css')?>" />
  <link rel="stylesheet" href="<?=base_url('assets/mag/css/style.css')?>" />
  <link rel="shortcut icon" href="<?=base_url('assets/mag/img/favicon.ico')?>">
  <link rel="apple-touch-icon" href="<?=base_url('assets/mag/img/apple-touch-icon.php')?>">
  <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('assets/mag/img/apple-touch-icon-72x72.png')?>">
  <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/mag/img/apple-touch-icon-114x114.png')?>">
  <script src="<?=base_url('assets/mag/js/lazysizes.min.js')?>"></script>
</head>

<body class="bg-light style-default style-rounded">

  <!-- Preloader -->
  <div class="loader-mask">
    <div class="loader">
      <div></div>
    </div>
  </div>

  <!-- Bg Overlay -->
  <div class="content-overlay"></div>

  <?php include('includes/sidenav.php'); ?>

  <main class="main oh" id="main">

  <?php include('includes/top_bar.php'); ?>

  <?php include('includes/header.php'); ?>
    
  <?php include('includes/newsflash.php'); ?>
  

    <!-- Featured Posts Grid -->      
    <section class="featured-posts-grid">
      <div class="container">
        <div class="row row-8">

          <div class="col-lg-6">

            <!-- Small post -->
            <div class="featured-posts-grid__item featured-posts-grid__item--sm">
              <article class="entry card post-list featured-posts-grid__entry">
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url(<?=base_url('images/post/'.$image1)?>)">
                  <a href="<?=base_url('Magazine/single_post/'.$value1->post_id)?>" class="thumb-url"></a>
                  <img src="<?=base_url('images/post/'.$image1)?>" alt="" class="entry__img d-none">
                  <a href="categories.php" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet"><?=$cat_name1?></a>
                </div>

                <div class="entry__body post-list__body card__body">  
                  <h2 class="entry__title">
                    <a href="<?=base_url('Magazine/single_post/'.$value1->post_id)?>"><?=$title1?></a>
                  </h2>
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>by</span>
                      <a href="#">Admin</a>
                    </li>
                    <li class="entry__meta-date">
                      Jan 21, 2018
                    </li>              
                  </ul>
                </div>
              </article>
            </div> <!-- end post -->

            <!-- Small post -->
            <div class="featured-posts-grid__item featured-posts-grid__item--sm">
              <article class="entry card post-list featured-posts-grid__entry">
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url('<?=base_url('images/post/'.$image2)?>')">
                  <a href="<?=base_url('Magazine/single_post/'.$value2->post_id)?>" class="thumb-url"></a>
                  <img src="<?=base_url('images/post/'.$image2)?>" alt="asdgs" class="entry__img d-none">
                  <a href="categories.php" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple"><?=$cat_name2?></a>
                </div>

                <div class="entry__body post-list__body card__body">  
                  <h2 class="entry__title">
                    <a href="<?=base_url('Magazine/single_post/'.$value2->post_id)?>"><?=$title2?></a>
                  </h2>
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>by</span>
                      <a href="#">Admin</a>
                    </li>
                    <li class="entry__meta-date">
                      Jan 21, 2018
                    </li>              
                  </ul>
                </div>
              </article>
            </div> <!-- end post -->

            <!-- Small post -->
            <div class="featured-posts-grid__item featured-posts-grid__item--sm">
              <article class="entry card post-list featured-posts-grid__entry">
                <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url('<?=base_url('images/post/'.$image3)?>')">
                  <a href="<?=base_url('Magazine/single_post/'.$value3->post_id)?>" class="thumb-url"></a>
                  <img src="<?=base_url('images/post/'.$image3)?>'" alt="" class="entry__img d-none">
                  <a href="categories.php" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--blue"><?=$cat_name3?></a>
                </div>

                <div class="entry__body post-list__body card__body">  
                  <h2 class="entry__title">
                    <a href="<?=base_url('Magazine/single_post/'.$value3->post_id)?>"><?=$title3?></a>
                  </h2>
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>by</span>
                      <a href="#">Admin</a>
                    </li>
                    <li class="entry__meta-date">
                      Jan 21, 2018
                    </li>              
                  </ul>
                </div>
              </article>
            </div> <!-- end post -->

          </div> <!-- end col -->

          <div class="col-lg-6">

            <!-- Large post -->
            <div class="featured-posts-grid__item featured-posts-grid__item--lg">
              <article class="entry card featured-posts-grid__entry">
                <div class="entry__img-holder card__img-holder">
                  <a href="<?=base_url('Magazine/single_post/'.$value4->post_id)?>">
                    <img src="<?=base_url('images/post/'.$image4)?>" alt="" class="entry__img">
                  </a>
                  <a href="categories.php" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green"><?=$cat_name4?></a>
                </div>

                <div class="entry__body card__body">   
                  <h2 class="entry__title">
                    <a href="<?=base_url('Magazine/single_post/'.$value4->post_id)?>"><?=$title4?></a>
                  </h2>
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>by</span>
                      <a href="#">Admin</a>
                    </li>
                    <li class="entry__meta-date">
                      Jan 21, 2018
                    </li>              
                  </ul>
                </div>
              </article>
            </div> <!-- end large post -->
          </div>          

        </div>
      </div>
    </section> <!-- end featured posts grid -->

    <div class="main-container container pt-24" id="main-container">         

      <!-- Content -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content">
          
          <!-- Latest News -->
          <section class="section tab-post mb-16">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">Latest News</h3>

              <div class="tabs tab-post__tabs"> 
                <ul class="tabs__list">
                  
                    
                    <?php 
                    foreach ($i as $latest) 
                    { ?>
                      <li class="tabs__item">
                        <a href="#cat<?=$latest->cat_id?>" class="tabs__trigger"><?=$latest->cat_name?></a>
                      </li>
                   <?php }
                    ?>

                </ul> <!-- end tabs -->
              </div>
            </div>

            <!-- tab content -->
            <div class="tabs__content tabs__content-trigger tab-post__tabs-content">
              
              <div class="tabs__content-pane tabs__content-pane--active" id="tab-all">
                <div class="row card-row">
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_1.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_2.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">fashion</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_3.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange">mining</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_4.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green">lifestyle</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">10 Horrible Habits You're Doing Right Now That Are Draining Your Energy</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </div> <!-- end pane 1 -->

              <div class="tabs__content-pane" id="tab-world">
                <div class="row card-row">
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_3.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange">mining</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_4.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green">lifestyle</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">10 Horrible Habits You're Doing Right Now That Are Draining Your Energy</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/mg/content/grid/grid_post_1.jpg')?>i" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_2.jpg"')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">fashion</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>                  
                </div>
              </div> <!-- end pane 2 -->

              <div class="tabs__content-pane" id="tab-lifestyle">
                <div class="row card-row">
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_1.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_2.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">fashion</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_3.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange">mining</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_4.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green">lifestyle</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">10 Horrible Habits You're Doing Right Now That Are Draining Your Energy</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </div> <!-- end pane 3 -->

              <div class="tabs__content-pane" id="tab-business">
                <div class="row card-row">
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_3.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange">mining</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_4.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green">lifestyle</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">10 Horrible Habits You're Doing Right Now That Are Draining Your Energy</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_1.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_2.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">fashion</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>                  
                </div>
              </div> <!-- end pane 4 -->

              <div class="tabs__content-pane" id="tab-fashion">
                <div class="row card-row">
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_1.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Follow These Smartphone Habits of Successful Entrepreneurs</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_2.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--purple">fashion</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_3.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange">mining</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                  <div class="col-md-6">
                    <article class="entry card">
                      <div class="entry__img-holder card__img-holder">
                        <a href="single_post.php">
                          <div class="thumb-container thumb-70">
                            <img data-src="<?=base_url('assets/mag/img/content/grid/grid_post_4.jpg')?>" src="<?=base_url('assets/mag/img/empty.png')?>" class="entry__img lazyload" alt="" />
                          </div>
                        </a>
                        <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green">lifestyle</a>
                      </div>

                      <div class="entry__body card__body">
                        <div class="entry__header">
                          
                          <h2 class="entry__title">
                            <a href="single_post.php">10 Horrible Habits You're Doing Right Now That Are Draining Your Energy</a>
                          </h2>
                          <ul class="entry__meta">
                            <li class="entry__meta-author">
                              <span>by</span>
                              <a href="#">Admin</a>
                            </li>
                            <li class="entry__meta-date">
                              Jan 21, 2018
                            </li>
                          </ul>
                        </div>
                        <div class="entry__excerpt">
                          <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                        </div>
                      </div>
                    </article>
                  </div>
                </div>
              </div> <!-- end pane 5 -->
            </div> <!-- end tab content -->            
          </section> <!-- end latest news -->

        </div> <!-- end posts -->

        <!-- Sidebar -->
        <aside class="col-lg-4 sidebar sidebar--right">

          <!-- Widget Popular Posts -->
          <aside class="widget widget-popular-posts">
            <h4 class="widget-title">Popular Posts</h4>
            <ul class="post-list-small">
              
              <?php
              foreach ($g as $post) 
              { ?>
              
                <li class="post-list-small__item">
                <article class="post-list-small__entry clearfix">
                  <div class="post-list-small__img-holder">
                    <div class="thumb-container thumb-100">
                      <a href="<?=base_url('Magazine/single_post/'.$post->post_id)?>">
                        <img data-src="<?=base_url('images/post/'.$post->image)?>" src="<?=base_url('images/post/'.$post->image)?>" alt="" class="post-list-small__img--rounded lazyload">
                      </a>
                    </div>
                  </div>
                  <div class="post-list-small__body">
                    <h3 class="post-list-small__entry-title">
                      <a href="<?=base_url('Magazine/single_post/'.$post->post_id)?>"><?=$post->title?></a>
                    </h3>
                    <ul class="entry__meta">
                      <li class="entry__meta-author">
                        <span>by</span>
                        <a href="#"><?=$post->user_name?></a>
                      </li> 
                      <li class="entry__meta-date">
                        <?=date('d-M-Y H:i A', strtotime($post->created_at))?>
                      </li>
                    </ul>
                  </div>                  
                </article>
              </li>

              <?php } ?>

              


            </ul>           
          </aside> <!-- end widget popular posts -->

          <!-- Widget Newsletter -->
          <aside class="widget widget_mc4wp_form_widget" style="display: none;">
            <h4 class="widget-title">Newsletter</h4>
            <p class="newsletter__text">
              <i class="ui-email newsletter__icon"></i>
              Subscribe for our daily news
            </p>
            <form class="mc4wp-form" method="post">
              <div class="mc4wp-form-fields">
                <div class="form-group">
                  <input type="email" name="EMAIL" placeholder="Your email" required="">
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-lg btn-color" value="Sign Up">
                </div>
              </div>
            </form>
          </aside> <!-- end widget newsletter -->

          <!-- Widget Socials -->
          <aside class="widget widget-socials">
            <h4 class="widget-title">Let's hang out on social</h4>
            <div class="socials socials--wide socials--large">
              <div class="row row-16">
                <div class="col">
                  <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                    <i class="ui-facebook"></i>
                    <span class="social__text">Facebook</span>
                  </a><!--
                  --><a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                    <i class="ui-twitter"></i>
                    <span class="social__text">Twitter</span>
                  </a><!--
                  --><a class="social social-youtube" href="#" title="youtube" target="_blank" aria-label="youtube">
                    <i class="ui-youtube"></i>
                    <span class="social__text">Youtube</span>
                  </a>
                </div>
                <div class="col">
                  <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                    <i class="ui-google"></i>
                    <span class="social__text">Google+</span>
                  </a><!--
                  --><a class="social social-instagram" href="#" title="instagram" target="_blank" aria-label="instagram">
                    <i class="ui-instagram"></i>
                    <span class="social__text">Instagram</span>
                  </a><!--
                  --><a class="social social-rss" href="#" title="rss" target="_blank" aria-label="rss">
                    <i class="ui-rss"></i>
                    <span class="social__text">Rss</span>
                  </a>
                </div>                
              </div>            
            </div>
          </aside> <!-- end widget socials -->

        </aside> <!-- end sidebar -->
  
      </div> <!-- end content -->

      <!-- Ad Banner 728 -->
      <div class="text-center pb-48">
        <a href="#">
          <img src="<?=base_url('assets/mag/img/content/placeholder_728.jpg')?>" alt="">
        </a>
      </div>

      <!-- Carousel posts -->
      <section class="section mb-0">
        <div class="title-wrap title-wrap--line title-wrap--pr">
          <h3 class="section-title">editors picks</h3>
        </div>

        <!-- Slider -->
        <div id="owl-posts" class="owl-carousel owl-theme owl-carousel--arrows-outside">
          
          <?php 
           foreach ($e as $ec) 
           {
             $cat_id1 = $ec->cat_id; 
             $cat_name1 = $ec->cat_name; 
             $title1 = $ec->title; 
             $description1 = $ec->description;
             $image1 = $ec->image; 
             ?>

             <article class="entry thumb thumb--size-1">
            <div class="entry__img-holder thumb__img-holder" style="background-image: url('<?=base_url('images/post/'.$ec->image)?>');">
              <div class="bottom-gradient"></div>
              <div class="thumb-text-holder">   
                <h2 class="thumb-entry-title">
                  <a href="<?=base_url('Magazine/single_post/'.$ec->post_id)?>"><?=$ec->title?></a>
                </h2>
              </div>
              <a href="<?=base_url('Magazine/single_post/'.$ec->post_id)?>" class="thumb-url"></a>
            </div>
          </article> 

           <?php } ?>

           




        </div> <!-- end slider -->

      </section> <!-- end carousel posts -->





    
      <!-- Content Secondary -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content mb-72">

          <!-- Worldwide News -->
          <section class="section">
            <div class="title-wrap title-wrap--line">
              <h3 class="section-title">Worldwide</h3>
              <a href="#" class="all-posts-url">View All</a>
            </div>

            <?php 
           foreach ($e as $ec) 
           {
             $cat_id1 = $ec->cat_id; 
             $cat_name1 = $ec->cat_name; 
             $title1 = $ec->title; 
             $description1 = $ec->description;
             $image1 = $ec->image; 
             ?>

             <article class="entry card post-list">
              <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url('<?=base_url('images/post/'.$ec->image)?>')">
                <a href="<?=base_url('Magazine/single_post/'.$ec->post_id)?>" class="thumb-url"></a>
                <img src="<?=base_url('images/post/'.$ec->image)?>" alt="" class="entry__img d-none">
                <a href="categories.php" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--blue"><?=$ec->cat_name?></a>
              </div>

              <div class="entry__body post-list__body card__body">
                <div class="entry__header">
                  <h2 class="entry__title">
                    <a href="<?=base_url('Magazine/single_post/'.$ec->post_id)?>"><?=$ec->title?></a>
                  </h2>
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>by</span>
                      <a href="#">Admin</a>
                    </li>
                    <li class="entry__meta-date">
                      Jan 21, 2018
                    </li>
                  </ul>
                </div>        
                <div class="entry__excerpt">
                  <p><?=substr($ec->description, 0, 70);?>... <a href="<?=base_url('Magazine/single_post/'.$ec->post_id)?>">Read More</a></p>
                </div>
              </div>
            </article>



        <?php } ?>

            

            

            
          </section> <!-- end worldwide news -->

          <!-- Pagination -->
          <nav class="pagination">
            <span class="pagination__page pagination__page--current">1</span>
            <a href="#" class="pagination__page">2</a>
            <a href="#" class="pagination__page">3</a>
            <a href="#" class="pagination__page">4</a>
            <a href="#" class="pagination__page pagination__icon pagination__page--next"><i class="ui-arrow-right"></i></a>
          </nav>

        </div> <!-- end posts -->

        <!-- Sidebar 1 -->
        <aside class="col-lg-4 sidebar sidebar--1 sidebar--right">

          <!-- Widget Ad 300 -->
          <aside class="widget widget_media_image">
            <a href="#">
              <img src="<?=base_url('assets/mag/img/content/placeholder_336.jpg')?>" alt="">
            </a>
          </aside> <!-- end widget ad 300 -->
          
          <!-- Widget Categories -->
          <aside class="widget widget_categories">
            <h4 class="widget-title">Categories</h4>
            <ul>
              <?php
               foreach ($f as $cat_name) 
               { ?> 
                 <li><a href="#"><?=$cat_name->cat_name?><span class="categories-count"><?=$cat_name->num_post?></span></a></li>
              <?php }
               ?>
            </ul>
          </aside> <!-- end widget categories -->

          <!-- Widget Recommended (Rating) -->
          <aside class="widget widget-rating-posts">
            <h4 class="widget-title">Recommended</h4>


            <?php
              foreach ($h as $recommended) 
              { ?>
                <article class="entry">
              <div class="entry__img-holder">
                <a href="<?=base_url('Magazine/single_post/'.$recommended->post_id)?>">
                  <div class="thumb-container thumb-60">
                    <img data-src="<?=base_url('images/post/'.$recommended->image)?>" src="<?=base_url('images/post/'.$recommended->image)?>" class="entry__img lazyload" alt="">
                  </div>
                </a>
              </div>

              <div class="entry__body">
                <div class="entry__header">
                  
                  <h2 class="entry__title">
                    <a href="<?=base_url('images/single_post/'.$recommended->post_id)?>"><?=$recommended->title?></a>
                  </h2>
                  <ul class="entry__meta">
                    <li class="entry__meta-author">
                      <span>by</span>
                      <a href="#"><?=$recommended->user_name?></a>
                    </li>
                    <li class="entry__meta-date">
                      <?=date('d-M-Y H:i A', strtotime($recommended->created_at))?>
                    </li>
                  </ul>
                  <ul class="entry__meta">
                    <li class="entry__meta-rating">
                      <i class="ui-star"></i><!--
                      --><i class="ui-star"></i><!--
                      --><i class="ui-star"></i><!--
                      --><i class="ui-star"></i><!--
                      --><i class="ui-star-empty"></i>
                    </li>
                  </ul>
                </div>
              </div>
            </article>
              <?php } ?>
            




          </aside> <!-- end widget recommended (rating) -->
        </aside> <!-- end sidebar 1 -->
      </div> <!-- content secondary -->      
      

    </div> <!-- end main container -->

   <?php include('includes/footer.php'); ?>

    <div id="back-to-top">
      <a href="#top" aria-label="Go to top"><i class="ui-arrow-up"></i></a>
    </div>

  </main> <!-- end main-wrapper -->

  
  <!-- jQuery Scripts -->
  <script src="<?=base_url('assets/mag/js/jquery.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/bootstrap.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/easing.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/owl-carousel.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/flickity.pkgd.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/twitterFetcher_min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/jquery.newsTicker.min.js')?>"></script>  
  <script src="<?=base_url('assets/mag/js/modernizr.min.js')?>"></script>
  <script src="<?=base_url('assets/mag/js/scripts.js')?>"></script>

</body>

</html>