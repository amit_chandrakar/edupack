  <aside class="main-sidebar">
    <!-- sidebar -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="image float-left">
          <img src="<?=base_url('images/user2-160x160.jpg')?>" class="rounded-circle" alt="User Image">
        </div>
        <div class="info float-left">
          <p style="color: black">Amit Chandrakar</p>
          <a href="#" style="color: black"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <!-- sidebar menu -->
      <ul class="sidebar-menu" data-widget="tree">
        
        <li <?php if($this->uri->uri_string()=='Admin_login/dashboard'){echo 'class="active"';}?>>
          <a href="<?=base_url('Admin_login/dashboard')?>">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
          </a>			
        </li>
		  
        <li <?php if($this->uri->uri_string()=='Admin_login/user'){echo 'class="active"';}?>>
          <a href="<?=base_url('Admin_login/user')?>">
            <i class="fa fa-users"></i>
            <span>Users</span>
          </a>
        </li>

        <li <?php if($this->uri->uri_string()=='Admin_login/category'){echo 'class="active"';}?>>
          <a href="<?=base_url('Admin_login/category')?>">
            <i class="fa fa-list-alt"></i>
            <span>Category</span>
          </a>
        </li>

        <li <?php if($this->uri->uri_string()=='Admin_login/post'){echo 'class="active"';}?>>
          <a href="<?=base_url('Admin_login/post')?>">
            <i class="fa fa-files-o"></i>
            <span>Post</span>
          </a>
        </li>

        
        <li <?php if($this->uri->uri_string()=='Admin_login/calendar'){echo 'class="active"';}?>>
          <a href="<?=base_url('Admin_login/calendar')?>">
            <i class="fa fa-calendar"></i>
            <span>Calendar</span>
          </a>
        </li>

        
      </ul>
    <!-- sidebar menu -->
  
    </section>
  </aside>