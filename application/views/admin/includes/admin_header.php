<header class="main-header">
    <!-- Logo -->
    <a href="index-2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?=base_url('images/minimal.png')?>"  alt=""></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Magazine </b>Admin</span>
    </a>
    <!-- Header Navbar-->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		  <!-- User Account-->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=base_url('images/user2-160x160.jpg')?>" class="user-image rounded-circle" alt="User Image">
            </a>
            <ul class="dropdown-menu scale-up">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=base_url('images/user2-160x160.jpg')?>" class="float-left rounded-circle" alt="User Image">

                <p>
                  Amit Chandrakar
                  <small class="mb-5">amitchandrakar028@gmail.com</small>
                  <a href="#" class="btn btn-danger btn-sm">View Profile</a>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-block btn-primary"><i class="ion ion-locked"></i> Lock</a>
                </div>
                <div class="pull-right">
                  <a href="<?=base_url('Admin_login/logout')?>" class="btn btn-block btn-danger"><i class="ion ion-power"></i> Log Out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>