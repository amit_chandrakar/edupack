<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php include('includes/title.php'); ?>
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/font-awesome/css/font-awesome.min.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/Ionicons/css/ionicons.min.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/fullcalendar/dist/fullcalendar.min.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/fullcalendar/dist/fullcalendar.print.min.css')?>" media="print">
  	<link rel="stylesheet" href="<?=base_url('css/master_style.css')?>">
  	<link rel="stylesheet" href="<?=base_url('css/skins/_all-skins.css')?>">
  	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>

<body class="hold-transition skin-orange-light sidebar-mini">
<div class="wrapper">

  <?php include('includes/admin_header.php') ?>
  <?php include('includes/admin_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Calendar
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Calendar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h4 class="box-title">Draggable Events</h4>
            </div>
            <div class="box-body">
              <!-- the events -->
              <div id="external-events" >
                <div class="external-event text-green dot-outline" data-class="bg-green"><i class="fa fa-hand-o-right"></i>Lunch</div>
                <div class="external-event text-yellow dot-outline" data-class="bg-yellow"><i class="fa fa-hand-o-right"></i>Go home</div>
                <div class="external-event text-aqua dot-outline" data-class="bg-aqua"><i class="fa fa-hand-o-right"></i>Do homework</div>
                <div class="external-event text-purple dot-outline" data-class="bg-purple"><i class="fa fa-hand-o-right"></i>Work on UI design</div>
                <div class="external-event text-red dot-outline" data-class="bg-red"><i class="fa fa-hand-o-right"></i>Sleep tight</div>
              </div>
              <div class="event-fc-bt">
                <!-- checkbox -->
				<div class="checkbox margin-top-20">
					<input id="drop-remove" type="checkbox">
					<label for="drop-remove">
						Remove after drop
					</label>
				</div>
             	<a href="#" data-toggle="modal" data-target="#add-new-events" class="btn btn-lg btn-orange btn-block margin-top-10">
					<i class="ti-plus"></i> Add New Event
				</a>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-lg-9 col-md-12">
          <div class="box box-primary">
            <div class="box-body no-padding">
              <!-- THE CALENDAR -->
              <div id="calendar"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- BEGIN MODAL -->
		<div class="modal none-border" id="my-event">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><strong>Add Event</strong></h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
						<button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Add Category -->
		<div class="modal fade none-border" id="add-new-events">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title"><strong>Add</strong> a category</h4>
					</div>
					<div class="modal-body">
						<form>
							<div class="row">
								<div class="col-md-6">
									<label class="control-label">Category Name</label>
									<input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
								</div>
								<div class="col-md-6">
									<label class="control-label">Choose Category Color</label>
									<select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
										<option value="success">Success</option>
										<option value="danger">Danger</option>
										<option value="info">Info</option>
										<option value="primary">Primary</option>
										<option value="warning">Warning</option>
										<option value="inverse">Inverse</option>
									</select>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
						<button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- END MODAL -->
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
  <?php include('includes/admin_footer.php') ?>

  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

	<script src="<?=base_url('assets/vendor_components/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/popper/dist/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-ui/jquery-ui.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/fastclick/lib/fastclick.js')?>"></script>
	<script src="<?=base_url('js/template.js')?>"></script>
	<script src="<?=base_url('js/demo.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/moment/moment.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/fullcalendar/dist/fullcalendar.min.js')?>"></script>
	<script src="<?=base_url('js/pages/calendar.js')?>"></script>
</body>
</html>
