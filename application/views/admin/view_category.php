<?php
 foreach ($view as $value) 
 {
   $cat_id = $value->cat_id; 
   $cat_name = $value->cat_name; 
   $status = $value->status; 
   $created_by = $value->created_by; 
   $cover_image = $value->cover_image; 
 }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php include('includes/title.php'); ?>
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/Ionicons/css/ionicons.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('css/master_style.css')?>">
	<link rel="stylesheet" href="<?=base_url('css/skins/_all-skins.css')?>">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>
<body class="hold-transition skin-orange-light sidebar-mini">
<div class="wrapper">

  <?php include('includes/admin_header.php') ?>
  <?php include('includes/admin_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h2>
        Edit Category
      </h2>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Forms</a></li>
        <li class="breadcrumb-item active">Form Validation</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-body">
          <div class="row">
            <div class="col">
            	<form novalidate method="post" enctype="multipart/form-data" action="<?=base_url('Admin_login/update_category/'.$cat_id)?>">
					

          <div class="form-group">
						<h5>Category Name <span class="text-danger">*</span></h5>
						<div class="controls">
							<input disabled id="category" type="text" class="form-control" required data-validation-required-message="This field is required" name="cat_name" value="<?=$cat_name?>"> </div>
					</div>

          <div class="form-group">
            <h5>Category Status <span class="text-danger">*</span></h5>
            <div class="controls">
              <select disabled id="status" required class="form-control" name="cat_status">
                <option <?php if($status==1){echo "selected";}?> value="1">Active</option>
                <option <?php if($status==0){echo "selected";}?> value="0">Deactive</option>
              </select>
            </div>
          </div>

					<div class="form-group">
						<h5>Category Cover Image (<a target="_blank" href="<?=base_url('images/category/'.$cover_image)?>">View</a> )</h5>
						<div class="controls">
						<input type="file" name="cover_image" class="form-control"> </div>
					</div>
          
					<div class="text-xs-right">
						<button type="button" class="btn btn-warning btn-block" id="edit">Update</button>
            <button type="submit" class="btn btn-success btn-block" id="save_btn" style="display: none;">Save</button>
					</div>
				</form>
            	
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include('includes/admin_footer.php') ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

	<script src="<?=base_url('assets/vendor_components/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/popper/dist/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/fastclick/lib/fastclick.js')?>"></script>
	<script src="<?=base_url('js/template.js')?>"></script>
	<script src="<?=base_url('js/demo.js')?>"></script>
    <script src="<?=base_url('js/pages/validation.js')?>"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(), $(".skin-square input").iCheck({
            checkboxClass: "icheckbox_square-green",
            radioClass: "iradio_square-green"
        }), $(".touchspin").TouchSpin(), $(".switchBootstrap").bootstrapSwitch();
    }(window, document, jQuery);
    </script>

    <script>
        $("#edit").click(function(event)
        {
           event.preventDefault();
           $('#edit').hide();
           $('#save_btn').show();
           $('#category').prop("disabled", false);
           $('#status').prop("disabled", false);
        });
    </script>

</body>
</html>
