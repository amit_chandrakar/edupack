<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">
    <title>EduPack | Registration </title>
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/Ionicons/css/ionicons.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('css/master_style.css')?>">
	<link rel="stylesheet" href="<?=base_url('css/skins/_all-skins.css')?>">	
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <!-- <a href="index-2.html"><b>Minimal-Art </b>Admin</a> -->
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new membership</p>

    <form action="" method="post" class="form-element">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Full name">
        <span class="ion ion-person form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email">
        <span class="ion ion-email form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password">
        <span class="ion ion-locked form-control-feedback "></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Retype password">
        <span class="ion ion-log-in form-control-feedback "></span>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="checkbox">
            <input type="checkbox" id="basic_checkbox_1" >
			<label for="basic_checkbox_1">I agree to the <a href="#"><b>Terms</b></a></label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-12 text-center">
          <button type="submit" class="btn btn-orange btn-block btn-flat margin-top-10">SIGN UP</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
	
	<div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
      <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
    </div>
	<!-- /.social-auth-links -->
    
     <div class="margin-top-20 text-center">
    	<p>Already have an account?<a href="login.php" class="text-info m-l-5"> Sign In</a></p>
     </div>
    
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
	<script src="<?=base_url('assets/vendor_components/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/popper/dist/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
</body>
</html>
