﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php include('includes/title.php'); ?>
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/font-awesome/css/font-awesome.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/Ionicons/css/ionicons.css')?>">
  	<link rel="stylesheet" href="<?=base_url('css/master_style.css')?>">
  	<link rel="stylesheet" href="<?=base_url('css/skins/_all-skins.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/jvectormap/jquery-jvectormap.css')?>">
  	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/morris.js/morris.css')?>">
  	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>

<body class="hold-transition skin-orange-light sidebar-mini">
<div class="wrapper">

  <?php include('includes/admin_header.php') ?>
  <?php include('includes/admin_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xl-4 col-md-6 col-12">
          <div class="info-box">
            <span class="info-box-icon push-bottom bg-orange"><i class="ion ion-ios-pricetag-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Users</span>
              <span class="info-box-number"><?=$a?></span>

              <div class="progress">
                <div class="progress-bar bg-orange" style="width: 45%"></div>
              </div>
              <!-- <span class="progress-description text-muted">45% Increase in 28 Days</span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-4 col-md-6 col-12">
          <div class="info-box">
            <span class="info-box-icon push-bottom bg-orange"><i class="ion ion-ios-eye-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Category</span>
              <span class="info-box-number"><?=$b?></span>

              <div class="progress">
                <div class="progress-bar bg-orange" style="width: 40%"></div>
              </div>
              <!-- <span class="progress-description text-muted">40% Increase in 28 Days</span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-xl-4 col-md-6 col-12">
          <div class="info-box">
            <span class="info-box-icon push-bottom bg-orange"><i class="ion ion-ios-cloud-download-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Post</span>
              <span class="info-box-number"><?=$c?></span>

              <div class="progress">
                <div class="progress-bar bg-orange" style="width: 85%"></div>
              </div>
              <!-- <span class="progress-description text-muted">85% Increase in 28 Days</span> -->
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->			
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include('includes/admin_footer.php') ?>

  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
  	
	<script src="<?=base_url('assets/vendor_components/jquery/dist/jquery.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/popper/dist/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/bootstrap/dist/js/bootstrap.js')?>"></script>	
	<script src="<?=base_url('assets/vendor_components/chart-js/chart.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js')?>"></script>
	<script src="<?=base_url('assets/vendor_plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>	
	<script src="<?=base_url('assets/vendor_plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>	
	<script src="<?=base_url('assets/vendor_components/raphael/raphael.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/morris.js/morris.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/fastclick/lib/fastclick.js')?>"></script>
	<script src="<?=base_url('js/template.js')?>"></script>
	<script src="<?=base_url('js/pages/dashboard.js')?>"></script>
	<script src="<?=base_url('js/demo.js')?>"></script>
</body>
</html>
