<?php
 foreach ($view as $value) 
 {
   $post_id = $value->post_id; 
   $title = $value->title; 
   $description = $value->description; 
   $status = $value->status; 
   $cat_id = $value->cat_id; 
   $created_by = $value->created_by; 
 }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php include('includes/title.php'); ?>
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/font-awesome/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('assets/vendor_components/Ionicons/css/ionicons.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('css/master_style.css')?>">
	<link rel="stylesheet" href="<?=base_url('css/skins/_all-skins.css')?>">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- CKRDITOR -->
    <link rel="stylesheet" href="<?=base_url('ckeditor/samples/css/samples.css')?>">
    <link rel="stylesheet" href="<?=base_url('ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css')?>">
  <!-- CKRDITOR -->
</head>
<body class="hold-transition skin-orange-light sidebar-mini">
<div class="wrapper">

  <?php include('includes/admin_header.php') ?>
  <?php include('includes/admin_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h2>Read Post</h2>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?=base_url('Admin_login/dashboard')?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="<?=base_url('Admin_login/post')?>">Post</a></li>
        <li class="breadcrumb-item active">Read Post</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     
     <!-- Basic Forms -->
      <div class="box box-default">
        <div class="box-body">
          <div class="row">
            <div class="col">
            	<form novalidate method="post" enctype="multipart/form-data" action="<?=base_url('Admin_login/update_post/'.$post_id)?>">
					<div class="form-group">
						<h5>Title <span class="text-danger">*</span></h5>
						<div class="controls">
							<input id="title" disabled type="text" name="title" class="form-control" required data-validation-required-message="This field is required" value="<?=$title?>" > </div>
					</div>

					<div class="form-group">
						<h5>Category Name <span class="text-danger">*</span></h5>
						<div class="controls">
							<select required class="form-control" name="category" id="category" disabled>
              <?php
              foreach ($cat as $row) 
              { ?>
								<option <?php if($row->cat_id == $cat_id) ?> value="<?php echo $row->cat_id;?>"><?php echo $row->cat_name;?></option>
              <?php } ?>
							</select>
						</div>
					</div>



          <div class="form-group">
            <h5>Status <span class="text-danger">*</span></h5>
            <div class="controls">
              <select required class="form-control" name="status" id="status" disabled>
              <option <?php if($status==1){echo "selected";}?> value="1">Activate</option>
              <option <?php if($status==0){echo "selected";}?> value="0">Dectivate</option>
              </select>
            </div>
          </div>

					<div class="form-group">
						<h5>Cover Image<span class="text-danger">*</span></h5>
						<div class="controls">
						<input type="file" name="image" class="form-control data-validation-required-message="This field is required" > </div>
					</div>

          <div class="form-group">
            <textarea id="editor" name="description" class="form-control"><?=$description?></textarea>
          </div>
					
					<div class="text-xs-right">
            <button type="button" class="btn btn-warning btn-block" id="edit">Update</button>
						<button type="submit" class="btn btn-success btn-block" id="save_btn" style="display: none;">Save</button>
					</div>
				</form>
            	
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include('includes/admin_footer.php') ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
  
  


	<script src="<?=base_url('assets/vendor_components/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/popper/dist/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/fastclick/lib/fastclick.js')?>"></script>
	<script src="<?=base_url('js/template.js')?>"></script>
	<script src="<?=base_url('js/demo.js')?>"></script>
    <script src="<?=base_url('js/pages/validation.js')?>"></script>
    <script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(), $(".skin-square input").iCheck({
            checkboxClass: "icheckbox_square-green",
            radioClass: "iradio_square-green"
        }), $(".touchspin").TouchSpin(), $(".switchBootstrap").bootstrapSwitch();
    }(window, document, jQuery);
    </script>

    <!-- CKEDITOR -->
    <script src="<?=base_url('ckeditor/ckeditor.js')?>"></script>
    <script src="<?=base_url('ckeditor/samples/js/sample.js')?>"></script>
    <script>
      initSample();
    </script>
    <!-- CKEDITOR -->

    <script>
        $("#edit").click(function(event)
        {
           event.preventDefault();
           $('#edit').hide();
           $('#save_btn').show();
           $("#title").prop("disabled", false);
           $('#category').prop("disabled", false);
           $('#status').prop("disabled", false);
        });
    </script>

</body>
</html>
