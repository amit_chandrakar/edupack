<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <?php include('includes/title.php'); ?>
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/font-awesome/css/font-awesome.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/Ionicons/css/ionicons.css')?>">
  <link rel="stylesheet" href="<?=base_url('css/master_style.css')?>">
  <link rel="stylesheet" href="<?=base_url('css/skins/_all-skins.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/jvectormap/jquery-jvectormap.css')?>">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
  <style type="text/css">
  #return-to-top {
    position: fixed;
    bottom: 63px;
    right: 20px;
    background: #ff6028;
    width: 50px;
    height: 50px;
    display: block;
    text-decoration: none;
    -webkit-border-radius: 35px;
    -moz-border-radius: 35px;
    border-radius: 35px;
    -webkit-transition: all 0.3s linear;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;
    z-index: 500;
  }
  #return-to-top i {
      color: #fff;
      margin: 0;
      position: relative;
      left: 0px;
      top: 9px;
      font-size: 19px;
      -webkit-transition: all 0.3s ease;
      -moz-transition: all 0.3s ease;
      -ms-transition: all 0.3s ease;
      -o-transition: all 0.3s ease;
      transition: all 0.3s ease;
  }
  #return-to-top:hover {
      background: #ffbf36;
  }
  #return-to-top:hover i {
      color: #fff;
      /*top: 5px;*/
      -webkit-transform: rotate(360000deg);
    -ms-transform: rotate(360000deg);
    transform: rotate(360000deg);
  }
</style>
</head>

<body class="hold-transition skin-orange-light sidebar-mini">
<div class="wrapper">

  <!-- Return to Top -->
<a href="<?=base_url('Admin_login/new_category')?>" title="Add Category" id="return-to-top" class="btn btn-warning"><i class="fa fa-plus" ></i></a>

  <?php include('includes/admin_header.php') ?>
  <?php include('includes/admin_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Category List
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Tables</a></li>
        <li class="breadcrumb-item active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
         
         <div class="box">
            <div class="box-header">
              <div class="row">

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>Category</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
            </div>

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>Status</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Active</option>
                  <option>Deactive</option>
                </select>
              </div>
            </div>

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>From Date</label>
                <input type="text" class="form-control pull-right" id="datepicker">
              </div>
            </div>

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>To Date</label>
                <input type="text" class="form-control pull-right" id="datepicker1">
              </div>
            </div>

            <div class="col-md-4 col-12" style="margin-top: 30px;">
              <div class="form-group">
                <input type="submit" name="Search" value="Search" class="btn btn-lg btn-success">
                <input type="submit" name="reset" value="Reset" class="btn btn-lg btn-warning">
                <button type="button" class="btn btn-danger btn-lg">Cancel</button>
              </div>
            </div>

          </div>
            </div>
          </div>
          <!-- /.box -->
         
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
				<thead>
					<tr>
						<th>SN</th>
            <th>Categoty Name</th>
						<th>Status</th>
						<th>Created By</th>
            <th>Created_at</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					 <?php
      $i=1;
        foreach ($h->result() as $row) 
        { ?>
            <tr>
              <td><?=$i?></td>
              <td><?php echo $row->cat_name;?></td>
              <td><?php if($row->status=='0'){echo "<span class='label label-danger'>Deactivate</span>";}else{echo "<span class='label label-success'>Activate</span>";} ?></td>
              <!-- <td><?php echo $row->created_by;?></td> -->
              <td>Amit Chandrakar</td>
              <td><?php echo date('d-m-Y H:i A', strtotime($row->created_at));?></td>
              <td>
                <!-- <a href="<?=base_url('Admin_login/edit_emp/'.$row->id)?>" class="btn btn-warning">Edit</a> -->
                <a href="<?=base_url('Admin_login/view_category/'.$row->cat_id)?>" class="btn btn-warning">Edit</a>
                <a href="<?=base_url('Admin_login/delete_category/'.$row->cat_id)?>" class="btn btn-danger">Delete</a>
              </td>
            </tr>
       <?php $i++; } ?>
				</tbody>
			</table>

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include('includes/admin_footer.php') ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

	<script src="<?=base_url('assets/vendor_components/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/popper/dist/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/fastclick/lib/fastclick.js')?>"></script>
	<script src="<?=base_url('js/template.js')?>"></script>
	<script src="<?=base_url('js/demo.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js')?>"></script>
	<script src="<?=base_url('js/pages/data-table.js')?>"></script>
</body>
</html>
