<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <?php include('includes/title.php'); ?>
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/font-awesome/css/font-awesome.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/Ionicons/css/ionicons.css')?>">
  <link rel="stylesheet" href="<?=base_url('css/master_style.css')?>">
  <link rel="stylesheet" href="<?=base_url('css/skins/_all-skins.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/vendor_components/jvectormap/jquery-jvectormap.css')?>">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
</head>

<body class="hold-transition skin-orange-light sidebar-mini">
<div class="wrapper">

  <?php include('includes/admin_header.php') ?>
  <?php include('includes/admin_sidebar.php') ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users List
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="breadcrumb-item"><a href="#">Tables</a></li>
        <li class="breadcrumb-item active">Data tables</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
         
         <div class="box">
            <div class="box-header">
              <div class="row">

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>User Name</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
            </div>

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>Status</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Active</option>
                  <option>Deactive</option>
                </select>
              </div>
            </div>

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>From Date</label>
                <input type="text" class="form-control pull-right" id="datepicker">
              </div>
            </div>

            <div class="col-md-2 col-12">
              <div class="form-group">
                <label>To Date</label>
                <input type="text" class="form-control pull-right" id="datepicker1">
              </div>
            </div>

            <div class="col-md-4 col-12" style="margin-top: 30px;">
              <div class="form-group">
                <input type="submit" name="Search" value="Search" class="btn btn-lg btn-success">
                <input type="submit" name="reset" value="Reset" class="btn btn-lg btn-warning">
                <button type="button" class="btn btn-danger btn-lg">Cancel</button>
              </div>
            </div>

          </div>
            </div>
          </div>
          <!-- /.box -->
         
          <div class="box">
            <div class="box-body">
              <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
				<thead>
					<tr>
						<th>Name</th>
						<th>User Type</th>
						<th>Contact</th>
						<th>Email</th>
						<th>Verified</th>
            <th>Join Date</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Brielle Williamson</td>
						<td>User</td>
						<td>9826485847</td>
						<td>user@gmail.com</td>
            <td><span class="label label-success">Yes</span></td>
						<td>2012/12/02</td>
						<td>Edit | Delete</td>
					</tr>
				</tbody>
			</table>

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include('includes/admin_footer.php') ?>
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

	<script src="<?=base_url('assets/vendor_components/jquery/dist/jquery.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/popper/dist/popper.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/datatables.net/js/jquery.dataTables.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js')?>"></script>
	<script src="<?=base_url('assets/vendor_components/fastclick/lib/fastclick.js')?>"></script>
	<script src="<?=base_url('js/template.js')?>"></script>
	<script src="<?=base_url('js/demo.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js')?>"></script>
  <script src="<?=base_url('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js')?>"></script>
	<script src="<?=base_url('js/pages/data-table.js')?>"></script>
</body>
</html>
