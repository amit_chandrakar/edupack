<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Magazine extends CI_Controller 
{
	public function __construct()
	{
	    parent:: __construct();
	    $this->load->Model('Magazine_model');
	}
	public function index()
	{
		$data['a'] = $this->Magazine_model->fetch_news1();
		$data['b'] = $this->Magazine_model->fetch_news2();
		$data['c'] = $this->Magazine_model->fetch_news3();
		$data['d'] = $this->Magazine_model->fetch_news4();
		$data['e'] = $this->Magazine_model->editor_pick();
		$data['f'] = $this->Magazine_model->get_category_name();
		$data['g'] = $this->Magazine_model->popular_post();
		$data['h'] = $this->Magazine_model->recommended_post();
		$data['i'] = $this->Magazine_model->latest_news();
		$data['j'] = $this->Magazine_model->footer_popular_post();
		$this->load->view('mag/index', $data);
	}
	public function about()
	{
		$data['f'] = $this->Magazine_model->get_category_name();
		$data['j'] = $this->Magazine_model->footer_popular_post();
		$this->load->view('mag/about', $data);
	}
	public function contact()
	{
		$data['f'] = $this->Magazine_model->get_category_name();
		$data['j'] = $this->Magazine_model->footer_popular_post();
		$this->load->view('mag/contact', $data);
	}
	public function category($cat_id)
	{
		$data['g'] = $this->Magazine_model->popular_post();
		$data['j'] = $this->Magazine_model->footer_popular_post();
		$data['e'] = $this->Magazine_model->editor_pick();
		$data['f'] = $this->Magazine_model->get_category_name();
		$data['aaa'] = $this->Magazine_model->get_category_news($cat_id);
		$data['bbb'] = $this->Magazine_model->get_single_category_name($cat_id);
		$this->load->view('mag/category', $data);
	}
	public function single_post($post_id)
	{
		$data['n'] = $this->Magazine_model->next_post($post_id);
		$data['p'] = $this->Magazine_model->previous_post($post_id);
		$data['f'] = $this->Magazine_model->get_category_name();
		$data['j'] = $this->Magazine_model->footer_popular_post();
		$data['a'] = $this->Magazine_model->post_detail($post_id);
		foreach ($data['a'] as $value) 
		{
			$cat_id = $value->cat_id;
		}
		$data['b'] = $this->Magazine_model->similar_post($cat_id);
		$data['c'] = $this->Magazine_model->popular_post();
		$this->load->view('mag/single_post', $data);
	}
	public function subscribe()
	{
		$email = $this->input->post('email');
		$data = $this->Magazine_model->subscribe($email);
		if ($data) 
		{
			$this->session->set_flashdata('subscribe', 'success');
			return redirect('Magazine/index');
		}
		else
		{
			$this->session->set_flashdata('subscribe', 'fail');
			return redirect('Magazine/index');
		}
	}


}/*End of class*/