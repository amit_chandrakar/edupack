<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_login extends CI_Controller 
{
	public function __construct()
	{
	    parent:: __construct();
	    $this->load->Model('Admin_model');
	    $this->load->Model('Magazine_model');
	    // if(! $this->session->userdata('id'))
	    // {
	    // 	return redirect('Admin_login/login');
	    // }
	}
	public function login()
	{
		$this->load->view('admin/login');
	}
	public function login_logic()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$id = $this->Admin_model->login_logic($username, $password);
		if ($id) 
		{
			$this->session->set_userdata('id',$id);
			$this->dashboard();
			return redirect('Admin_login/dashboard');
			// echo "Login success";
		}
		else
		{
			$this->session->set_flashdata('login_status','Fail');
			return redirect('Admin_login/login');
		}
	}
	public function register()
	{
		$this->load->view('admin/register');
	}
	public function dashboard()
	{
		if(! $this->session->userdata('id'))
	    {
	    	return $this->login();
	    }
		$this->db->where(['verified'=>1, 'user_type'=>'U']);
		$num_rows['a'] = $this->db->count_all_results('users');

		$this->db->where(['status'=>1]);
		$num_rows['b'] = $this->db->count_all_results('category');

		$this->db->where(['status'=>1]);
		$num_rows['c'] = $this->db->count_all_results('post');

		$this->load->view('admin/index', $num_rows);
	}
	public function calendar()
	{
		$this->load->view('admin/calendar');
	}
	public function category()
	{
		$data['h'] = $this->Admin_model->fetch_category();
		$this->load->view('admin/category', $data);
		// $this->load->view('admin/category');
	}
	public function new_category()
	{
		$this->load->view('admin/new_category');
	}
	public function new_post()
	{
		$category['cat'] = $this->Admin_model->populate_dropdown();
		$this->load->view('admin/new_post', $category);
		// $this->load->view('admin/new_post');
	}
	public function post()
	{
		$data['h'] = $this->Admin_model->fetch_post();
		$this->load->view('admin/post', $data);
	}
	public function user()
	{
		$this->load->view('admin/users');
	}
	public function save_new_category()
	{
		$cat_name = $this->input->post('cat_name');
		$cat_status = $this->input->post('cat_status');
		$config['upload_path'] = './images/category/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']     = '';
		$config['max_width'] = '';
		$config['max_height'] = '';
		$this->load->library('upload', $config);
		$return_data = $this->upload->do_upload('cover_image');
		$upload_data = $this->upload->data();
		if($return_data)
		{
			$id = $this->Admin_model->save_new_category($cat_name, $cat_status, $upload_data['file_name'] );
			if ($id) 
			{
				$this->category();
			}
			else
			{
				$this->category();
			}
		}
	}
	public function populate_dropdown()
	{
		$category['cat'] = $this->Admin_model->populate_dropdown();
		$this->load->view('admin/new_post', $category);
	}
	public function save_new_post()
	{
		$title = $this->input->post('title');
		$category = $this->input->post('category');
		$description = $this->input->post('description');
		$status = $this->input->post('status');

		$config['upload_path'] = './images/post/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']     = '';
		$config['max_width'] = '';
		$config['max_height'] = '';
		$this->load->library('upload', $config);
		$return_data = $this->upload->do_upload('image');
		$upload_data = $this->upload->data();
		if($return_data)
		{
			// $config['file_name']=$category.rand(0000,9999);
			$id = $this->Admin_model->save_new_post($title, $category, $description, $status,  $upload_data['file_name'] );
			if ($id) 
			{
				$from = 'amitchandrakar028@gmail.com';
				$this->load->library('email');
				$data['a'] = $this->Admin_model->get_subscribed_user();
				foreach ($data['a'] as $value) 
				{
					$this->email->set_newline("\r\n");
			        $this->email->from($from);
			        $this->email->to($value->email);
			        $this->email->subject($title);
			        $this->email->message($description);
			        $this->email->send();
				}
				$this->post();
			}
			else
			{
				$this->post();
			}
		}
	}

	public function view_post($post_id)
	{
		$post['view'] = $this->Admin_model->view_post($post_id);
		$post['cat'] = $this->Admin_model->populate_dropdown();
		$this->load->view('admin/view_post', $post);
	}


	public function update_post($post_id)
	{
		$title = $this->input->post('title');
		$category = $this->input->post('category');
		$description = $this->input->post('description');
		$status = $this->input->post('status');
		$id = $this->Admin_model->update_post($title, $category, $description, $status, $post_id);
		if ($id) 
		{
			$this->post();
		}
		else
		{
			$this->post();
		}
	}


	public function delete_post($post_id)
	{
		$id = $this->Admin_model->delete_post($post_id);
		if ($id) 
		{
			$this->post();
		}
		else
		{
			$this->post();
		}
	}




	public function view_category($cat_id)
	{
		$post['view'] = $this->Admin_model->view_category($cat_id);
		$post['cat'] = $this->Admin_model->populate_dropdown();
		$this->load->view('admin/view_category', $post);
	}


	public function update_category($cat_id)
	{
		$cat_name = $this->input->post('cat_name');
		$cat_status = $this->input->post('cat_status');
		$id = $this->Admin_model->update_category($cat_name, $cat_status, $cat_id);
		if ($id) 
		{
			$this->category();
		}
		else
		{
			$this->category();
		}
	}


	public function delete_category($cat_id)
	{
		$id = $this->Admin_model->delete_category($cat_id);
		if ($id) 
		{
			$this->category();
		}
		else
		{
			$this->category();
		}
	}

	public function demo()
	{
		$this->db->where(['verified'=>1, 'user_type'=>'U']);
		$num_rows = $this->db->count_all_results('users');
		$this->load->view('Admin_login/index', $num_rows);
	}

	public function logout()
	{
		$this->session->unset_userdata('id');
		return redirect('Admin_login/login');
	}










}/*End of class*/