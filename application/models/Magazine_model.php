<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Magazine_model extends CI_Model
{
	public function fetch_news1()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.cat_id'=>1, 'category.status'=>1, 'post.status'=>1]);
		$this->db->order_by('post_id', 'desc');
		 $this->db->limit(1); 
		$query = $this->db->get();
		return $query->result();
	}
	public function fetch_news2()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.cat_id'=>2, 'category.status'=>1, 'post.status'=>1]);
		 $this->db->order_by('post_id', 'desc');
		 $this->db->limit(1); 
		$query = $this->db->get();
		return $query->result();
	}
	public function fetch_news3()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.cat_id'=>3, 'category.status'=>1, 'post.status'=>1]);
		$this->db->order_by('post_id', 'desc');
		 $this->db->limit(1); 
		$query = $this->db->get();
		return $query->result();
	}
	public function fetch_news4()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.cat_id'=>4, 'category.status'=>1, 'post.status'=>1]);
		$this->db->order_by('post_id', 'desc');
		 $this->db->limit(1); 
		$query = $this->db->get();
		return $query->result();
	}
	public function editor_pick()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.status'=>1, 'post.status'=>1]);
		// $this->db->order_by('post_id', 'desc');
		 // $this->db->limit(1); 
		$query = $this->db->get();
		return $query->result();
	}
	public function get_category_name()
	{
		
		$this->db->select('*, category.cat_name, COUNT(post.post_id) as num_post');
		$this->db->from('category');
		$this->db->join('post', 'category.cat_id = post.cat_id', 'INNER');
		$this->db->where(['category.status'=>1]);
		$this->db->group_by('category.cat_name');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_category_news($cat_id)
	{
		
		$this->db->select('*');
		$this->db->from('category');
		$this->db->join('post', 'category.cat_id = post.cat_id');
		$this->db->join('users', 'users.userid = post.created_by');
		$this->db->where(['category.status'=>1, 'post.status'=>1, 'category.cat_id'=>$cat_id]);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_single_category_name($cat_id)
	{
		
		$this->db->select('*');
		$this->db->from('category');
		$this->db->where(['category.cat_id'=>$cat_id]);
		$query = $this->db->get();
		return $query->result();
	}
	public function post_detail($post_id)
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->join('users', 'users.userid = post.created_by');
		$this->db->where(['category.status'=>1, 'post.status'=>1, 'post.post_id'=>$post_id]);
		// $this->db->order_by('post_id', 'desc');
		 // $this->db->limit(1); 
		$query = $this->db->get();
		

		return $query->result();
	}
	public function similar_post($cat_id)
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.status'=>1, 'post.status'=>1, 'post.cat_id'=>$cat_id]);
		$query = $this->db->get();
		return $query->result();
	}
	public function popular_post()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->join('users', 'users.userid = post.created_by');
		$this->db->where(['category.status'=>1, 'post.status'=>1]);
		$this->db->order_by('post_id', 'desc');
		$this->db->limit(4); 
		$query = $this->db->get();
		return $query->result();
	}
	public function footer_popular_post()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->join('users', 'users.userid = post.created_by');
		$this->db->where(['category.status'=>1, 'post.status'=>1]);
		$this->db->order_by('post_id', 'desc');
		$this->db->limit(2); 
		$query = $this->db->get();
		return $query->result();
	}
	public function recommended_post()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->join('users', 'users.userid = post.created_by');
		$this->db->where(['category.status'=>1, 'post.status'=>1]);
		$this->db->order_by('post_id', 'desc');
		$this->db->limit(2); 
		$query = $this->db->get();
		return $query->result();
	}
	public function latest_news()
	{
		
		$this->db->select('*');
		$this->db->from('category');
		$this->db->where(['category.status'=>1]);
		$this->db->order_by('cat_id', 'asc');
		$this->db->limit(5); 
		$query = $this->db->get();
		return $query->result();
	}
	public function subscribe($email)
	{
		$data = array(
        'email'=> $email,
        'status '=> 1,
        'subscribe_date'=> date('Y-m-d H:i:s')
    	);
		$result = $this->db->insert('subscribe',$data);
		if ($result) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function next_post($post_id)
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.status'=>1, 'post.status'=>1, 'post.post_id!='.$post_id]);
		$this->db->order_by('post.post_id', 'desc');
		 $this->db->limit(1); 
		$query = $this->db->get();
		return $query->result();
	}

	public function previous_post($post_id)
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$this->db->where(['category.status'=>1, 'post.status'=>1, 'post.post_id!='.$post_id]);
		$this->db->order_by('post.post_id', 'asc');
		 $this->db->limit(1); 
		$query = $this->db->get();
		return $query->result();
	}
	

} /*End of main class*/