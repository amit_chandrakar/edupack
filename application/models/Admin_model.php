<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model
{
	public function login_logic($username, $password)
	{
		$query = $this->db->where(['user_name'=>$username, 'password'=>$password])->get('users');
		if($query->num_rows())
		{
			return $query->row()->userid;
		}
		else
		{
			return false;
		}
	}



	public function save_new_category($cat_name, $cat_status, $image)
	{
		$data = array(
        'cat_name'=> $cat_name,
        'status '=> $cat_status,
        'cover_image'=> $image,
        'created_at'=> date('Y-m-d H:i:s')
    	);
		$result = $this->db->insert('category',$data);
		if ($result) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	public function fetch_category()
	{
		$query = $this->db->get('category');
		return $query;
	}

	public function fetch_post()
	{
		$this->db->select('*');
		$this->db->from('post');
		$this->db->join('category', 'post.cat_id = category.cat_id');
		$query = $this->db->get();
		return $query;
	}


	public function populate_dropdown()
	{
		$this->db->where(['status'=>1]);
		$query = $this->db->get('category');
		return $query->result();
	}

	public function save_new_post($title, $category, $description, $status, $image)
	{
		$data = array(
        'cat_id'=> $category,
        'title '=> $title,
        'description '=> $description,
        'image'=> $image,
        'status'=> $status,
        'created_by'=> '1',
        'created_at'=> date('Y-m-d H:i:s')
    	);
		$result = $this->db->insert('post',$data);
		if ($result) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function view_post($post_id)
	{
		$this->db->where(['post_id'=>$post_id]);
		$query = $this->db->get('post');
		return $query->result();
	}

	public function update_post($title, $category, $description, $status, $post_id)
	{
		$data = array(
        'cat_id'=> $category,
        'title '=> $title,
        'description '=> $description,
        'status'=> $status,
        'modified_at'=> date('Y-m-d H:i:s')
    	);
		$this->db->where('post_id', $post_id);  
		$result = $this->db->update('post', $data);  
		if ($result) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete_post($post_id)
	{
		$this->db->where('post_id', $post_id);  
		$result = $this->db->delete('post');    
		if ($result) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}








	public function view_category($cat_id)
	{
		$this->db->where(['cat_id'=>$cat_id]);
		$query = $this->db->get('category');
		return $query->result();
	}

	public function update_category($cat_name, $cat_status ,$cat_id)
	{
		$data = array(
        'cat_name'=> $cat_name,
        'status '=> $cat_status,
        'modified_at'=> date('Y-m-d H:i:s')
    	);
		$this->db->where('cat_id', $cat_id);  
		$result = $this->db->update('category', $data);  
		if ($result) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function delete_category($cat_id)
	{
		$this->db->where('cat_id', $cat_id);  
		$result = $this->db->delete('category');    
		if ($result) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_subscribed_user()
	{
		
		$this->db->select('*');
		$this->db->from('subscribe');
		$this->db->where(['status'=>1]);
		$query = $this->db->get();
		return $query->result();
	}











} /*End of main class*/